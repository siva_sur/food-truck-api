/**
 * 
 */
package com.foodTruck.demo.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.foodTruck.demo.constant.FoodTruckConstants;
import com.foodTruck.demo.entity.FoodTruck;
import com.foodTruck.demo.entity.GeoLocation;
import com.foodTruck.demo.exception.model.ModelToEntityMappingExcpetion;
import com.foodTruck.demo.information.FoodTruckInformation;
import com.foodTruck.demo.information.TruckDataInformation;

/**
 * @author pc
 *
 */
public class FoodTruckUtil {

	public static FoodTruck mapInformationToEntity(TruckDataInformation info, FoodTruck data) throws Exception{
		try {
				data.setApplicant(info.getApplicant());
				data.setBlock(info.getBlock());
				data.setBlockLot(info.getBlocklot());
				data.setCnn(Integer.parseInt(info.getCnn()));
				data.setExpirationdate(info.getExpirationdate());
				data.setFacilityType(info.getFacilitytype());
				data.setId(Integer.parseInt(info.getObjectid()));
				data.setLocation(new GeoLocation());
				data.getLocation().setLatitude(Double.parseDouble(info.getLatitude()));
				data.getLocation().setLongitude(Double.parseDouble(info.getLongitude()));
				data.getLocation().setType(FoodTruckConstants.EntityConstants.LOCATION_TYPE_POINT);
				data.setLot(info.getLot());
				data.setPermit(info.getPermit());
				data.setPriorpermit(Integer.parseInt(info.getPriorpermit()));
				data.setReceivedDate(info.getReceived());
				data.setSchedule(info.getSchedule());
				data.setStatus(info.getStatus());
				data.setStreet(info.getAddress());
		}catch(Exception e) {
			throw new ModelToEntityMappingExcpetion("There was an issue with mapping the data: "+e.getStackTrace());
		}
				
		return data;
		
	}
	
	public static FoodTruck mapInformationToEntity(FoodTruckInformation info, FoodTruck data) throws Exception{
		try {
				data.setApplicant(info.getApplicant());
				data.setBlock(info.getBlock());
				data.setBlockLot(info.getBlockLot());
				data.setCnn(info.getCnn());
				data.setExpirationdate(info.getExpirationdate());
				data.setFacilityType(info.getFacilityType());
				//data.setId(Integer.parseInt(info.getObjectid()));
				data.setLocation(new GeoLocation());
				data.getLocation().setLatitude(info.getLocation().getLatitude());
				data.getLocation().setLongitude(info.getLocation().getLongitude());
				data.getLocation().setType(FoodTruckConstants.EntityConstants.LOCATION_TYPE_POINT);
				data.setLot(info.getLot());
				data.setPermit(info.getPermit());
				data.setPriorpermit(info.getPriorpermit());
				data.setReceivedDate(info.getReceivedDate());
				data.setSchedule(info.getSchedule());
				data.setStatus(info.getStatus());
				data.setStreet(info.getStreet());
		}catch(Exception e) {
			throw new ModelToEntityMappingExcpetion("There was an issue with mapping the data: "+e.getStackTrace());
		}
				
		return data;
		
	}
	
//	public static Map<String,Object> findFieldsForUpdate(FoodTruckInformation information,FoodTruck foodTruck){
//		Map<String,Object> fieldMap = new LinkedHashMap<String, Object>();
//		if(null!=information.getApplicant() && null!=foodTruck.getApplicant()){
//			if(!information.getApplicant().equalsIgnoreCase(foodTruck.getApplicant())) {
//				fieldMap.put("applicant", information.getApplicant());
//			}
//		}else if (foodTruck.getApplicant() == null){
//			
//		}
//		if(!information.getStreet().equalsIgnoreCase(foodTruck.getStreet())) {
//			fieldMap.put("street", information.getStreet());
//		}
//		
//		if(!information.getBlock().equalsIgnoreCase(foodTruck.getBlock())) {
//			fieldMap.put("block", information.getBlock());
//		}
//		
//		if(!information.getBlockLot().equalsIgnoreCase(foodTruck.getBlockLot())) {
//			fieldMap.put("blockLot", information.getBlockLot());
//		}
//		
//		if(!information.getLot().equalsIgnoreCase(foodTruck.getLot())) {
//			fieldMap.put("lot", information.getLot());
//		}
//		
//		if(information.getCnn() != foodTruck.getCnn()) {
//			fieldMap.put("cnn", information.getCnn());
//		}
//		
//		if(!information.getFacilityType().equalsIgnoreCase(foodTruck.getFacilityType())) {
//			fieldMap.put("facilityType", information.getFacilityType());
//		}
//		
//		if(!information.getPermit().equalsIgnoreCase(foodTruck.getPermit())) {
//			fieldMap.put("permit", information.getFacilityType());
//		}
//		
//		if(!information.getStatus().equalsIgnoreCase(foodTruck.getStatus())) {
//			fieldMap.put("status", information.getStatus());
//		}
//		
//		if(!information.getSchedule().equalsIgnoreCase(foodTruck.getSchedule())) {
//			fieldMap.put("schedule", information.getSchedule());
//		}
//		
//		if(!information.getReceivedDate().equalsIgnoreCase(foodTruck.getReceivedDate())) {
//			fieldMap.put("receivedDate", information.getReceivedDate());
//		}
//		
//		if(information.getPriorpermit() != foodTruck.getPriorpermit()){
//			fieldMap.put("priorpermit",	information.getPriorpermit());
//		}
//		
//		if(!information.getExpirationdate().equalsIgnoreCase(foodTruck.getExpirationdate())){
//			fieldMap.put("expirationdate", information.getExpirationdate());
//		}
//		
//		return fieldMap;
//	}
}
