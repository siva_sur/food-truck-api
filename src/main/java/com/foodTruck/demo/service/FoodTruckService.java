package com.foodTruck.demo.service;
/*
 * @author Siva
 * 
 * The following service contains the methods that will 
 * be implemented by the service impl class. All the methods here relate 
 * to queries on the foodTruck collection
 */
import java.util.List;

import org.springframework.stereotype.Service;

import com.foodTruck.demo.entity.FoodTruck;
import com.foodTruck.demo.information.FoodTruckInformation;
import com.foodTruck.demo.information.TruckDataInformation;

@Service
public interface FoodTruckService {

	public boolean foodTruckDataLoad(List<TruckDataInformation> information) throws Exception;
	public List<FoodTruck> findAll();
	public List<FoodTruck> findByApplicant(String applicant) throws Exception;
	public List<FoodTruck> findByStreet(String street) throws Exception;
	public List<FoodTruck> findByExpirationDate(String date) throws Exception;
	public List<FoodTruck> findByExpirationDateGreater(String date) throws Exception;
	public List<FoodTruck> findNearestTruck(Double longitude, Double latitude) throws Exception;
	public boolean deleteByPermit(String permit) throws Exception;
	public boolean upsertData(FoodTruckInformation information) throws Exception;
	
}
