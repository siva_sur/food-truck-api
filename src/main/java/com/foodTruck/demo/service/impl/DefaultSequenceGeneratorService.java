/**
 * 
 */
package com.foodTruck.demo.service.impl;


import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.foodTruck.demo.entity.DatabaseSequences;
import com.foodTruck.demo.exception.model.GeneralRepositoryTransactionFailureException;
import com.foodTruck.demo.service.SequenceGeneratorService;

/**
 * @author pc
 *
 */
@Component
public class DefaultSequenceGeneratorService implements SequenceGeneratorService {

	@Autowired
	MongoOperations mongoOperations; 
	/**
	 * The generate sequence method uses the mongoOperations method to query and upsert a new 
	 * sequene value anytime a new record is added to the document. This method is called from 
	 * any instance where a new object is being created and requires the generation of a new 
	 * sequence value
	 */
	@Override
	public long generateSequence(String sequenceName) throws Exception{
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(sequenceName));
			DatabaseSequences counter = mongoOperations.findAndModify(query,
			new Update().inc("seq",1),new FindAndModifyOptions().returnNew(true).upsert(true),
			DatabaseSequences.class);
			return !Objects.isNull(counter) ? counter.getSeq() : 1;
		}catch(Exception e) {
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
	}

}
