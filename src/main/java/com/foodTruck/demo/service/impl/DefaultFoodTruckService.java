/**
 * 
 */
package com.foodTruck.demo.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.foodTruck.demo.constant.FoodTruckConstants;
import com.foodTruck.demo.entity.FoodTruck;
import com.foodTruck.demo.exception.model.GeneralRepositoryTransactionFailureException;
import com.foodTruck.demo.information.FoodTruckInformation;
import com.foodTruck.demo.information.TruckDataInformation;
import com.foodTruck.demo.repository.FoodTruckRepository;
import com.foodTruck.demo.service.FoodTruckService;
import com.foodTruck.demo.util.FoodTruckUtil;
import com.mongodb.client.result.UpdateResult;

/**
 * @author Siva
 * 
 * This class contains the default implementation of 
 * the services.
 *
 */

 @Component
public class DefaultFoodTruckService implements FoodTruckService {

	 @Autowired
	 FoodTruckRepository foodTruckRepository;
	 
	 @Autowired
	 MongoOperations operations;

	 /**
	  * This is the service that is used initially to load information into the database from
	  * the dataset provided through 'data.sfgov.org'. This method will map the required information
	  * from the information object and set it to the FoodTruck object which represents the collection
	  * that we will persist for the use case.
	  */
	@Override
	public boolean foodTruckDataLoad(List<TruckDataInformation> information) throws Exception {
		FoodTruck foodTruck = new FoodTruck();
		for(TruckDataInformation info:information) {
		foodTruck = FoodTruckUtil.mapInformationToEntity(info,foodTruck);
		foodTruckRepository.save(foodTruck);
		}
		return true;
	}
	/**
	 * Method returns all records in the database
	 */
	@Override
	public List<FoodTruck> findAll() {
		// TODO Auto-generated method stub
		return foodTruckRepository.findAll();
	}
	/**
	 * This method returns all records based on the following conditions;
	 * 1. applicant name should be 'like' the value of applicant which is the param to this method.
	 * 2. The status of the applicant must be in approved status. This ensures that only those with valid passes will be returned.
	 * 3. The dataset returned is then sorted by their expiration date in an ascending order and returned.  
	 */
	@Override
	public List<FoodTruck> findByApplicant(String street) throws Exception{
		try {
				return foodTruckRepository.findByApplicantLikeIgnoreCaseAndStatusOrderByExpirationdateAsc(street,FoodTruckConstants.LicenseStatus.APPROVED);
		}catch(Exception e){
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
	}
	
	@Override
	public List<FoodTruck> findByStreet(String street) throws Exception{
		try {
				return foodTruckRepository.findByStreetLikeIgnoreCaseAndStatusOrderByExpirationdateAsc(street,FoodTruckConstants.LicenseStatus.APPROVED);
		}catch(Exception e){
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
	}

	/**
	 * This method takes expiration date and queries for the given date.
	 * The input date should be in the following format;
	 * "yyyy-MM-dd'T'HH:mm:ss.SSS"
	 */
	@Override
	public List<FoodTruck> findByExpirationDate(String date) throws Exception{
		try {
			return foodTruckRepository.findByExpirationdate(date);
		}catch(Exception e) {
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
	}

	/**
	 * This method takes expiration date and queries for any record whose date falls on a later date.
	 * The input date should be in the following format;
	 * "yyyy-MM-dd'T'HH:mm:ss.SSS"
	 */
	@Override
	public List<FoodTruck> findByExpirationDateGreater(String date) throws Exception {
		try {
		return foodTruckRepository.findByExpirationdateGreaterThanOrderByExpirationdateAsc(date);
		}catch(Exception e) {
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
	}
	/**
	 * This method returns the best possible truck for service as its response.
	 * The method accepts geo-spatial cordinates as inputs.
	 * Then for a set radius of about 50 miles, it queries for all records that 
	 * fall in the 50 mile radius.
	 * The query then ensures that these records are also having a license status as 
	 * approved to ensure that the food trucks have an actual approved permit for the same.
	 * 
	 * The queried result set is then returned as a response.
	 */
	@Override
	public List<FoodTruck> findNearestTruck(Double longitude, Double latitude) throws Exception {
		Point point = new Point(longitude,latitude);
		Distance distance = new Distance(50,Metrics.MILES);
		try {
		return foodTruckRepository.findByLocationNearAndStatus(point,distance,FoodTruckConstants.LicenseStatus.APPROVED);
		}
		catch(Exception e ) {
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
	}
	@Override
	public boolean deleteByPermit(String permit) throws Exception {
		try {
		return foodTruckRepository.deleteByPermit(permit);
		}
		catch(Exception e ) {
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
	}
	
	//This method, takes in a few parameters and upserts the document
	@Override
	public boolean upsertData(FoodTruckInformation information) throws Exception {
//			FoodTruck data = foodTruckRepository.findByPermit(information.getPermit());
			FoodTruck data = new FoodTruck(); 
//			Map<String,Object> updateFields = new LinkedHashMap<String,Object>();
//			updateFields = FoodTruckUtil.findFieldsForUpdate(information,data);
//			Query query = new Query();
//			query.addCriteria(Criteria.where("permit").is(information.getPermit()));
//			Update update = new Update();
//	        if(updateFields.isEmpty()) {
//	        	return false;
//	        }
//	        else {
//	        	for(Entry<String, Object> entry: updateFields.entrySet()) {
//	        		update.set(entry.getKey(), entry.getValue());
//	        	}
//	             }
//	        UpdateResult updates = operations.upsert(query, update, FoodTruck.class);
//	        if(updates.getModifiedCount()>0) {
//	        	return true;
//	        }
//	        else return false;
		data = FoodTruckUtil.mapInformationToEntity(information,data);
		try {
		foodTruckRepository.save(data);
		}catch(Exception e) {
			throw new GeneralRepositoryTransactionFailureException("Transaciton failure");
		}
		return true;

	}
	 
}
