package com.foodTruck.demo.service.impl;
/**
 * @author Siva
 * 
 * This is the default service implementation of the WebHookService interface.
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.foodTruck.demo.constant.FoodTruckConstants;
import com.foodTruck.demo.entity.FoodTruck;
import com.foodTruck.demo.entity.WebHookEntity;
import com.foodTruck.demo.exception.model.GeneralRepositoryTransactionFailureException;
import com.foodTruck.demo.exception.model.InvalidInputException;
import com.foodTruck.demo.information.WebHookInformation;
import com.foodTruck.demo.repository.FoodTruckRepository;
import com.foodTruck.demo.repository.WebHookRepository;
import com.foodTruck.demo.service.SequenceGeneratorService;
import com.foodTruck.demo.service.WebHookService;

@Component
public class DefaultWebHookService implements WebHookService{

	@Autowired
	SequenceGeneratorService generatorService;
	
	@Autowired
	WebHookRepository webHookRepository;

	@Autowired
	FoodTruckRepository foodTruckRepository;
	
	/**
	 * This is the method that will be invoked when a new webhook url is to be generated and 
	 * used. As you can see from the method, anytime a new generate request comes,
	 * the WebhookEntity object is instantiated and a new Id is updated.
	 * This entity is then updated with a base URL to which the generated Id is also 
	 * appended, making sure that everytime webHook URL will stay unique.
	 * 
	 * This webhook entity is then persisted and consuming application can now use this URL to
	 * provide data for expiration of the food truck data.
	 */
	@Override
	public WebHookInformation registerWebHook() throws Exception{
		
		WebHookInformation information = new WebHookInformation();
		WebHookEntity data = new WebHookEntity();
		data.setId(generatorService.generateSequence(WebHookEntity.SEQUENCE_NAME));
		information.setUrl(FoodTruckConstants.WebHook.BASE_URL+"id/"+data.getId()+"/autoExpiry/");
		data.setUrl(information.getUrl());
		try {
		data = webHookRepository.save(data);
		}catch(Exception e) {
			throw new GeneralRepositoryTransactionFailureException("The transaction execution has failed: "+e.getStackTrace());
		}
		return information;
	}

	/**
	 * This service method is what will be invoked when the registered url is 
	 * consumed by the applications at the other end of the hook. The other application
	 * is expected to send a list of FoodTruckIds. These Ids are then used to query 
	 * for those records in the database . The query will check if the Id exists and if it does,
	 * will then check if its status is in expired state.
	 * 
	 * If the status is already expired, the record is ignored and it continues on to the next record.
	 * If it finds a record which has a valid id and license status which has not expired,
	 * it will then validate the date of expiration against the current date and update 
	 * the entity accordingly.
	 *
	 */
	@Override
	public int autoExpireFoodTrucks(List<Long> truckId) throws Exception {
		int updateCount=0;
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		Date today = new  Date();
		if(truckId.size()>0) {
			for(long id : truckId) {
				if(id >0) {
					FoodTruck foodTruck = foodTruckRepository.findByIdAndStatusNotIn(id,FoodTruckConstants.LicenseStatus.EXPIRED);
					if(foodTruck!= null) {
						if(null != foodTruck.getExpirationdate()) {
							try {
							Date expDate = format.parse(foodTruck.getExpirationdate());
							if(today.compareTo(expDate)	>0) {
								System.out.println("Updating status of "+foodTruck.getId()+" whose status is :"+foodTruck.getStatus());
								foodTruck.setStatus(FoodTruckConstants.LicenseStatus.EXPIRED);
								foodTruckRepository.save(foodTruck);
								System.out.println("Updated status of "+foodTruck.getId()+" whose status is now :"+foodTruck.getStatus());
								updateCount++;
							}}catch(ParseException e) {
								throw new InvalidInputException("The expiration date could not be parsed: "+e.getStackTrace());
							}catch(Exception e) {
								throw new GeneralRepositoryTransactionFailureException("The Transaction could not be executed: "+e.getStackTrace());
							}
						}
					}
					
				}else {
					throw new InvalidInputException("An invalid Id was pulled ");
				}
			}
			return updateCount;
		}
		else return -1;
	}
	/**
	 * This method is used to validate if the id exists in the database.
	 */
	@Override
	public boolean validateId(long id) throws Exception{
		try {
		return webHookRepository.existsById(id);
		}catch(Exception e) {
			throw new InvalidInputException("An error has occured: "+e.getStackTrace());
		}
	}

}
