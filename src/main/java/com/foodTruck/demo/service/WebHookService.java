/**
 * 
 */
package com.foodTruck.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.foodTruck.demo.information.WebHookInformation;

/**
 * @author Siva
 * 
 * The webhook service interface has 3 methods,
 * registerWebhook to generate a unique URL that can be used 
 * by another application to send it information.
 * 
 * autoExpireFoodTrucks method to automatically expire any trucks 
 * that have an expiry date which has been passed.
 * 
 * validateId method is used to ensure that a correct id is being 
 * passed into the system.
 *
 */
@Service
public interface WebHookService {
	WebHookInformation registerWebHook() throws Exception;
	
	int autoExpireFoodTrucks(List<Long> foodTruckId) throws Exception;

	boolean validateId(long id) throws Exception;
}
