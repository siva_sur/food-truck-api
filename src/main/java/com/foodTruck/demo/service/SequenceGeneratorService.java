package com.foodTruck.demo.service;

import org.springframework.stereotype.Service;

/**
 * @author Siva
 * 
 * Service layer for the generator method that will
 * create the ids to uniquely identify each element being updated in the database.
 *
 */

@Service
public interface SequenceGeneratorService {

	long generateSequence(String sequenceName) throws Exception;
}
