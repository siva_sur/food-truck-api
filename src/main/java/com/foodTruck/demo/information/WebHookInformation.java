package com.foodTruck.demo.information;
/*
 * This is the class the is used to provide the application
 * with a request to generate a webhook URL.
 */

public class WebHookInformation {
	
	private String url;
	private String key;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}

}
