/**
 * 
 */
package com.foodTruck.demo.information;

/**
 * @author Siva
 * This holds the location information from the incoming dataset for the 
 * class TruckDataInformation.
 *
 */
public class Location {

	private String longitude;
	private String latitude;
	private String humanAddess;
	
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getHumanAddess() {
		return humanAddess;
	}
	public void setHumanAddess(String humanAddess) {
		this.humanAddess = humanAddess;
	}
	
	
}
