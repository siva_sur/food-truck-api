/**
 * 
 */
package com.foodTruck.demo.information;

/**
 * @author Siva
 * This class holds the geospatial location information for the foodTruck class
 *
 */
public class GeoLocationInformation {

	private double longitude;
	private double latitude;
	private String type;
	
	
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
