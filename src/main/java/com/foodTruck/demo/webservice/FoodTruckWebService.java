/**
 * 
 */
package com.foodTruck.demo.webservice;

import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodTruck.demo.entity.FoodTruck;
import com.foodTruck.demo.exception.model.InvalidInputException;
import com.foodTruck.demo.information.FoodTruckInformation;
import com.foodTruck.demo.information.TruckDataInformation;
import com.foodTruck.demo.service.impl.DefaultFoodTruckService;

/**
 * @author pc
 *Controller layer for the application
 */

@RestController
@RequestMapping("/foodTruck")
public class FoodTruckWebService {
	
	
	@Autowired
	MongoOperations operations;
	
	@Autowired
	DefaultFoodTruckService foodTruckService;
	
	@GetMapping("/getAllRecords")
	public List<FoodTruck> getAllData(){
		return foodTruckService.findAll();
	}
	
	@GetMapping("/findByApplicant/{name}")
	public List<FoodTruck> findByApplicant(@PathVariable String name) throws Exception{
		if(name.isEmpty() || name.equalsIgnoreCase(" ")) {
			throw new InvalidInputException("Applicant name should not be empty");
		}
		return foodTruckService.findByApplicant(name);
	}
	
	@GetMapping("/findByStreet/{street}")
	public List<FoodTruck> findByStreet(@PathVariable String street) throws Exception{
		if(street.isEmpty() || street.equalsIgnoreCase(" ")) {
			throw new InvalidInputException("Street name should not be empty");
		}
		return foodTruckService.findByApplicant(street);
	}
	
	@GetMapping("/findByExpirationDate/{date}")
	public List<FoodTruck> findExpirationDate(@PathVariable String date) throws Exception{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		String dateForService = format.format(format2.parse(date));
		if(date.isEmpty() || date.equalsIgnoreCase(" ")) {
			throw new InvalidInputException("Date should be provided");
		}
		try {
			format.parse(dateForService);
		}catch(Exception e) {
			throw new InvalidInputException("Date not in proper format"); 
		}
		return foodTruckService.findByExpirationDate(date);
	}
	
	@GetMapping("/findByExpirationDateGreaterThan/{date}")
	public List<FoodTruck> findByExpirationDateGreaterThan(@PathVariable String date) throws Exception{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		String dateForService = format.format(format2.parse(date));
		if(date.isEmpty() || date.equalsIgnoreCase(" ")) {
			throw new InvalidInputException("Date should be provided");
		}
		try {
			 format.parse(dateForService);
		}catch(Exception e) {
			throw new InvalidInputException("Date not in proper format"); 
		}
		return foodTruckService.findByExpirationDateGreater(date);
	}
	
	@GetMapping("/findNextTruck/{longitude}/{latitude}")
	public List<FoodTruck> findNextTruck(@PathVariable Double longitude,@PathVariable Double latitude) throws Exception{
		if(longitude == null || latitude == null) {
			throw new InvalidInputException("Longitude or Latitude is not provided");
		}
		if(-180 > longitude || longitude >180) {
			throw new InvalidInputException("Incorrect value for longitude");
		}
		if(-90 > latitude || latitude >90) {
			throw new InvalidInputException("Incorrect value for latitude");
		}
		return foodTruckService.findNearestTruck(longitude, latitude);
	}
	
	@DeleteMapping("/deletePermit/{permit}")
	public boolean deleteFoodTruckData(@PathVariable String permit) throws Exception {
		if(permit.isBlank() || permit == null) {
			throw new InvalidInputException("Permit Id should exists");
		}
		
		return foodTruckService.deleteByPermit(permit);
		
	}
	
	@PostMapping("/upsert/name/{name}/permit/{permit}")
	public boolean deleteFoodTruckData(@RequestBody FoodTruckInformation information) throws Exception {
		if(information == null) {
			throw new InvalidInputException("information object should exists");
		}
		return foodTruckService.upsertData(information);
	}
	@PostMapping("/loadFoodTruckData")
	public boolean loadFoodTruckData(@RequestBody List<TruckDataInformation> information) throws Exception{
		return foodTruckService.foodTruckDataLoad(information);
		
	}
}
