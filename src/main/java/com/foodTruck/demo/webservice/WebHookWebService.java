package com.foodTruck.demo.webservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foodTruck.demo.exception.model.InvalidInputException;
import com.foodTruck.demo.information.WebHookInformation;
import com.foodTruck.demo.service.WebHookService;
/**
 * 
 * @author Siva
 *
 *This is the controller layer for the webhook based request.
 *All webhook related requests will enter this controller.
 */
@RestController
@RequestMapping("/foodTruck/webHook")
public class WebHookWebService {
	
	@Autowired
	WebHookService webHookService;
	
	@GetMapping("/register")
	public WebHookInformation registerWebHook() throws Exception{
		
		return webHookService.registerWebHook();
		
	}
	
	@PostMapping("id/{id}/autoExpiry/")
	public int autoExpiryOfLicenses(@PathVariable long id,@RequestBody List<Long>foodTruckId) throws Exception {
		
			if(id>0) {
				if(webHookService.validateId(id)) {
						return webHookService.autoExpireFoodTrucks(foodTruckId);
				}
			}else {
				throw new InvalidInputException("Incorrect id passed");
		}
			return -1;
			
			
	}
}
