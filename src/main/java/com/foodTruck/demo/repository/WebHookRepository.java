package com.foodTruck.demo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.foodTruck.demo.entity.WebHookEntity;

/**
 * @author Siva
 * 
 * The following repository implementation contains the generated
 * queries that will be used to query the mongo Db.
 * The queries are pretty self explanatory.
 * This repository is used to access the collections being pointed to by the
 * webHookEntity entity.
 */
public interface WebHookRepository extends MongoRepository<WebHookEntity, Long> {

}
