/**
 * 
 */
package com.foodTruck.demo.repository;

import java.util.List;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.foodTruck.demo.constant.FoodTruckConstants;
import com.foodTruck.demo.entity.FoodTruck;


/**
 * @author Siva
 * 
 * The following repository implementation contains the generated
 * queries that will be used to query the mongo Db.
 * The queries are pretty self explanatory.
 * This repository is used to access the collections being pointed to by the
 * FoodTruck entity.
 *
 */
public interface FoodTruckRepository extends MongoRepository<FoodTruck, String> {
	
	List<FoodTruck> findByApplicantLikeIgnoreCaseAndStatusOrderByExpirationdateAsc(String name,String status);
	List<FoodTruck> findByStreetLikeIgnoreCaseAndStatusOrderByExpirationdateAsc(String street,String status);
	List<FoodTruck> findByExpirationdate(String date);
	List<FoodTruck> findByExpirationdateGreaterThanOrderByExpirationdateAsc(String date);
	List<FoodTruck> findByLocationNearAndStatus(Point point,Distance distance,String issued );
	FoodTruck findByIdAndStatusNotIn(long id,String status);
	boolean deleteByPermit(String permit);
	FoodTruck findByPermit(String permit);
}
