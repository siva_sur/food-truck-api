/**
 * 
 */
package com.foodTruck.demo.constant;

/**
 * @author Siva
 * 
 * The class holds constant values that are being referre to across
 * the applications.
 *
 */
public class FoodTruckConstants {

	public interface EntityConstants{
		public static final String LOCATION_TYPE_POINT = "Point";
	}
	
	public interface LicenseStatus{
		public static final String APPROVED = "APPROVED";
		public static final String REQUESTED = "REQUESTED";
		public static final String EXPIRED = "EXPIRED";
		public static final String ISSUED = "ISSUED";
		public static final String SUSPEND = "SUSPEND";
	}
	
	public interface WebHook{
		public static final String BASE_URL = "http://localhost:8080/foodTruck/webHook/";
	}
}
