/**
 * 
 */
package com.foodTruck.demo.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.foodTruck.demo.exception.model.GeneralRepositoryTransactionFailureException;
import com.foodTruck.demo.exception.model.InvalidInputException;
import com.foodTruck.demo.exception.model.ModelToEntityMappingExcpetion;

/**
 * @author Siva
 * 
 * Utilizing the global exception handling capability provided in 
 * spring framework by the use of @ControllerAdvice annotation.
 * For any exception thrown in the flow, the exceptions if handled will be 
 * caught here and appropriately returned in the response.
 *
 */
@ControllerAdvice
@RestController
public class FoodTruckResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	String invalidInput;
	
	
	@ExceptionHandler(InvalidInputException.class)
	public final ResponseEntity<Object> handleInvalidInput(InvalidInputException ex, 
			WebRequest request){
		return handleExceptionInternal(ex,ex.getMessage(), 
		          new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
		
	}
	
	@ExceptionHandler(GeneralRepositoryTransactionFailureException.class)
	public final ResponseEntity<Object> handleGeneralRepositoryFailureException(GeneralRepositoryTransactionFailureException ex, 
			WebRequest request){
		return handleExceptionInternal(ex,ex.getMessage(), 
		          new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
		
	}
	
	@ExceptionHandler(ModelToEntityMappingExcpetion.class)
	public final ResponseEntity<Object> handleModelToEntityMapping(ModelToEntityMappingExcpetion ex, 
			WebRequest request){
		return handleExceptionInternal(ex,ex.getMessage(), 
		          new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
		
	}
	
	@ExceptionHandler(RuntimeException.class)
	public final ResponseEntity<Object> handleModelToRuntimeExcpetion(RuntimeException ex, 
			WebRequest request){
		return handleExceptionInternal(ex,ex.getMessage(), 
		          new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
		
	}
	

}
