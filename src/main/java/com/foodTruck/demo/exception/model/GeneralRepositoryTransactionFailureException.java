package com.foodTruck.demo.exception.model;

public class GeneralRepositoryTransactionFailureException extends Exception {

	public GeneralRepositoryTransactionFailureException(String exception) {
		super(exception);
	}
}
