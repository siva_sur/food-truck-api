package com.foodTruck.demo.exception.model;

public class InvalidInputException extends Exception{

	public InvalidInputException(String exception) {
		super(exception);
	}
}
