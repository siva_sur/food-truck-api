/**
 * 
 */
package com.foodTruck.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Siva
 * This class is the entity mapped to the sequences document in the database.
 * Whenever an object is to be created, the sequences would have to be auto-generated
 * this is achieved through the use of this class and associated generator functions.
 *
 */

@Document(collection = "database_sequences")
public class DatabaseSequences {
	 @Id
	 private String id;
	 private long seq;
	 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public long getSeq() {
		return seq;
	}
	public void setSeq(long seq) {
		this.seq = seq;
	}
	 
}
