/**
 * 
 */
package com.foodTruck.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author Siva
 *This is the collection that holds the information about the webhooks that
 *are being created. For every webhook that is created, a unique URL will be formed 
 *onto which the id of the collection is attached. 
 */
@Document(collection = "webHookRegister")
public class WebHookEntity {
	
	@Transient
    public static final String SEQUENCE_NAME = "webhook_sequence";
	
	@Id
	private long id;
	private String url;
	//private String key;
	//private String uid;
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
//	public String getKey() {
//		return key;
//	}
//	public void setKey(String key) {
//		this.key = key;
//	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
//	public String getUid() {
//		return uid;
//	}
//	public void setUid(String uid) {
//		this.uid = uid;
//	}

}
