/**
 * 
 */
package com.foodTruck.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author pc
 * This is the actual data model that we will persist in the Database
 */

@Document(collection="foodTruckData")
public class FoodTruck {

	@Id
	private int id;
	private String  applicant;
	private String facilityType;
	private int cnn;
	private String street;
	private String blockLot;
	private String block;
	private String lot;
	private String permit;
	private String status;
	private String schedule;
	private String receivedDate;
	private int priorpermit;
	private String expirationdate;
	@GeoSpatialIndexed(name = "locationIndex",type = GeoSpatialIndexType.GEO_2DSPHERE)
	private GeoLocation location;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getApplicant() {
		return applicant;
	}
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}
	public String getFacilityType() {
		return facilityType;
	}
	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}
	public int getCnn() {
		return cnn;
	}
	public void setCnn(int cnn) {
		this.cnn = cnn;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getBlockLot() {
		return blockLot;
	}
	public void setBlockLot(String blockLot) {
		this.blockLot = blockLot;
	}
	public String getBlock() {
		return block;
	}
	public void setBlock(String block) {
		this.block = block;
	}
	public String getLot() {
		return lot;
	}
	public void setLot(String lot) {
		this.lot = lot;
	}
	public String getPermit() {
		return permit;
	}
	public void setPermit(String permit) {
		this.permit = permit;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSchedule() {
		return schedule;
	}
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	public String getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}
	public int getPriorpermit() {
		return priorpermit;
	}
	public void setPriorpermit(int priorpermit) {
		this.priorpermit = priorpermit;
	}
	public String getExpirationdate() {
		return expirationdate;
	}
	public void setExpirationdate(String expirationdate) {
		this.expirationdate = expirationdate;
	}
	public GeoLocation getLocation() {
		return location;
	}
	public void setLocation(GeoLocation location) {
		this.location = location;
	}
	
	
}
