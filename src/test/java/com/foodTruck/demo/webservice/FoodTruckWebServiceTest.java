package com.foodTruck.demo.webservice;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.web.servlet.MockMvc;

import com.foodTruck.demo.exception.model.InvalidInputException;
import com.foodTruck.demo.service.impl.DefaultFoodTruckService;

@SpringBootTest
public class FoodTruckWebServiceTest {
	
	public static String name = "Test";

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private FoodTruckWebService foodTruckWebService;
	
	@MockBean
	private MongoOperations operations;
	
	@MockBean
	private DefaultFoodTruckService foodTruckService;
	
	@Test 
	public void getAllDataMustReturnDataFromService() throws Exception{
		Mockito.when(foodTruckWebService.getAllData()).thenReturn(null);
		foodTruckWebService.getAllData();
		Assert.assertTrue(Integer.valueOf(1).equals(1));
	}
	
	@Test
	public void getAll() throws Exception{
		Mockito.when(foodTruckWebService.findByApplicant(name)).then(null);
		foodTruckWebService.findByApplicant(name);
		Assert.assertTrue(Integer.valueOf(1).equals(1));
	}
	
	@Test(expected = InvalidInputException.class)
	public void getAllWithNullName() throws Exception{
		Mockito.when(foodTruckWebService.findByApplicant(name)).then(null);
		foodTruckWebService.findByApplicant(null);
		Assert.assertTrue(Integer.valueOf(1).equals(1));
	}
}
